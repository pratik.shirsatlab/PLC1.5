EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:AUTOPLCV1.4-cache
EELAYER 25 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 2 10
Title ""
Date "1 dec 2016"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1600 2550 0    60   Input ~ 0
Processor_VCC
Text HLabel 1600 2700 0    60   Input ~ 0
Processor_GND
Text Label 1850 2550 0    60   ~ 0
3.3_V
Text Label 6050 9200 2    60   ~ 0
3.3_V
Text Label 6050 8350 2    60   ~ 0
Processor_GND
Text HLabel 4900 4350 0    60   Input ~ 0
P0.17_CTS1_MISO0_MISO_61
Text HLabel 4900 4500 0    60   Input ~ 0
P0.18_DCD1_MOSI0_MOSI_60
Text HLabel 5150 6050 0    60   Input ~ 0
P0.27_I2C0_SDA_25
Text HLabel 5150 6200 0    60   Input ~ 0
P0.28_I2C0SCL_24
Text HLabel 4950 2200 0    60   Input ~ 0
P0.2_AD7_UART0tx_98
Text HLabel 4950 2350 0    60   Input ~ 0
P0.3_AD6_UART0rx_99
Text HLabel 5000 3850 0    60   Input ~ 0
P0.15_TXD1_SCK0_SCK_62
Text HLabel 4950 4200 0    60   Input ~ 0
P0.16_UART1rx_slaveSEL_63
Text HLabel 5300 6350 0    60   Input ~ 0
P0.29_USB_D+_29
Text HLabel 5300 6500 0    60   Input ~ 0
P0.30_USB_D-_30
Text HLabel 11250 3600 2    60   Input ~ 0
P1.18_USBupLED_32
Text HLabel 11250 3750 2    60   Input ~ 0
P1.19_USB_PPWR_33
Text HLabel 11250 4200 2    60   Input ~ 0
P1.22_USB_PWRD_36
Text HLabel 11250 4950 2    60   Input ~ 0
P1.27_USBOVRCR_43
Text HLabel 4950 4650 0    60   Input ~ 0
P0.19_SDA1_59
Text HLabel 4950 4800 0    60   Input ~ 0
P0.20_SCL1_58
Text HLabel 5100 5250 0    60   Input ~ 0
P0.23_AD0_9
Text HLabel 5150 5600 0    60   Input ~ 0
P0.24_AD1_8
Text HLabel 5150 5750 0    60   Input ~ 0
P0.25_AD2_7
Text HLabel 5150 5900 0    60   Input ~ 0
P0.26_AD3_6_AOUT
Text HLabel 11250 5400 2    60   Input ~ 0
P1.30_AD4_Vbus_21
Text HLabel 11250 5550 2    60   Input ~ 0
P1.31_AD5_SCK1_20
Text Label 6050 2350 2    60   ~ 0
P0.3_AD6_UART0rx_99
Text Label 5800 5600 2    60   ~ 0
P0.24_AD1_8
Text Label 5800 5250 2    60   ~ 0
P0.23_AD0_9
Text Label 5700 4800 2    60   ~ 0
P0.20_SCL1_58
Text Label 5700 4650 2    60   ~ 0
P0.19_SDA1_59
Text Label 6150 4500 2    60   ~ 0
P0.18_DCD1_MOSI0_MOSI_60
Text Label 6150 4350 2    60   ~ 0
P0.17_CTS1_MISO0_MISO_61
Text Label 6150 3850 2    60   ~ 0
P0.15_TXD1_SCK0_SCK_62
Text Label 6150 4200 2    60   ~ 0
P0.16_UART1rx_slaveSEL_63
Text Label 6050 2200 2    60   ~ 0
P0.2_AD7_UART0tx_98
Text Label 5800 5750 2    60   ~ 0
P0.25_AD2_7
Text Label 5800 5900 2    60   ~ 0
P0.26_AD3_6_AOUT
Text Label 6100 6350 2    60   ~ 0
P0.29_USB_D+_29
Text Label 6100 6050 2    60   ~ 0
P0.27_I2C0_SDA_25
Text Label 6100 6200 2    60   ~ 0
P0.28_I2C0SCL_24
Text Label 6150 6500 2    60   ~ 0
P0.30_USB_D-_30
Text Label 1850 2700 0    60   ~ 0
Processor_GND
Text Label 10200 3600 0    60   ~ 0
P1.18_USBupLED_32
Text Label 10200 3750 0    60   ~ 0
P1.19_USB_PPWR_33
Text Label 10200 4200 0    60   ~ 0
P1.22_USB_PWRD_36
Text Label 10250 4950 0    60   ~ 0
P1.27_USBOVRCR_43
Text Label 10300 5400 0    60   ~ 0
P1.30_AD4_Vbus_21
Text Label 10300 5550 0    60   ~ 0
P1.31_AD5_SCK1_20
Text HLabel 5000 6850 0    60   Input ~ 0
P4.28_RXmCLK_TXD3_82
Text Label 6150 6850 2    60   ~ 0
P4.28_RXmCLK_TXD3_82
Text HLabel 5000 7000 0    60   Input ~ 0
P4.29_TXmCLK_RXD3_85
Text Label 6150 7000 2    60   ~ 0
P4.29_TXmCLK_RXD3_85
Text HLabel 5300 7350 0    60   Input ~ 0
P3.25_PWM1.2_27
Text Label 6100 7350 2    60   ~ 0
P3.25_PWM1.2_27
Text HLabel 5000 7500 0    60   Input ~ 0
P3.26_PWM1.3_STCLK_26
Text Label 6150 7500 2    60   ~ 0
P3.26_PWM1.3_STCLK_26
Text Label 6000 7750 2    60   ~ 0
3.3_V
NoConn ~ 6200 10300
Text HLabel 4950 2050 0    60   Input ~ 0
P0.1_RXD3_SCL1_47
Text Label 6050 2050 2    60   ~ 0
P0.1_RXD3_SCL1_47
Text HLabel 4950 1900 0    60   Input ~ 0
P0.0_TXD3_SDA1_46
Text Label 6050 1900 2    60   ~ 0
P0.0_TXD3_SDA1_46
Text Label 6100 2500 2    60   ~ 0
P0.4_I2SRXclk_81
Text HLabel 5250 2500 0    60   Input ~ 0
P0.4_I2SRXclk_81
Text HLabel 5250 2650 0    60   Input ~ 0
P0.5_I2SRXws_80
Text Label 6100 2650 2    60   ~ 0
P0.5_I2SRXws_80
Text HLabel 5350 2800 0    60   Input ~ 0
P0.6_SSEL1_79
Text Label 6100 2800 2    60   ~ 0
P0.6_SSEL1_79
Text HLabel 5350 2950 0    60   Input ~ 0
P0.7_SCK1_78
Text Label 6100 2950 2    60   ~ 0
P0.7_SCK1_78
Text HLabel 5350 3250 0    60   Input ~ 0
P0.8_MISO1_77
Text Label 6100 3250 2    60   ~ 0
P0.8_MISO1_77
Text HLabel 5350 3400 0    60   Input ~ 0
P0.9_MOSI1_76
Text Label 6100 3400 2    60   ~ 0
P0.9_MOSI1_76
Text HLabel 5050 3550 0    60   Input ~ 0
P0.10_TXD2_SDA2_48
Text Label 6050 3550 2    60   ~ 0
P0.10_TXD2_SDA2_48
Text HLabel 4950 3700 0    60   Input ~ 0
P0.11_RXD2_SCL2_49
Text Label 6050 3700 2    60   ~ 0
P0.11_RXD2_SCL2_49
Text HLabel 5650 4950 0    60   Input ~ 0
P0.21_57
Text Label 6100 4950 2    60   ~ 0
P0.21_57
Text HLabel 5650 5100 0    60   Input ~ 0
P0.22_56
Text Label 6100 5100 2    60   ~ 0
P0.22_56
Text HLabel 11000 1900 2    60   Input ~ 0
P1.0_ENTHtxd0_95
Text Label 10100 1900 0    60   ~ 0
P1.0_ENTHtxd0_95
Text Label 10100 2050 0    60   ~ 0
P1.1_ENTHtxd1_94
Text HLabel 11000 2050 2    60   Input ~ 0
P1.1_ENTHtxd1_94
Text Label 10100 2200 0    60   ~ 0
P1.4_ENTHtx_en_93
Text HLabel 11000 2200 2    60   Input ~ 0
P1.4_ENTHtx_en_93
Text Label 10100 2350 0    60   ~ 0
P1.8_ENTH_CRS_92
Text HLabel 11000 2350 2    60   Input ~ 0
P1.8_ENTH_CRS_92
Text Label 10100 2500 0    60   ~ 0
P1.9_ENTHrxd0_91
Text Label 10100 2650 0    60   ~ 0
P1.10_ENTHrxd1_90
Text Label 10100 2800 0    60   ~ 0
P1.14_ENTH_rxER_89
Text Label 10100 2950 0    60   ~ 0
P1.15_ENTHrefCLK_88
Text Label 10100 3100 0    60   ~ 0
P1.16_ENTH_MDC_87
Text Label 10100 3250 0    60   ~ 0
P1.17_ENTH_MDIO_86
Text HLabel 11100 3250 2    60   Input ~ 0
P1.17_ENTH_MDIO_86
Text HLabel 11100 3100 2    60   Input ~ 0
P1.16_ENTH_MDC_87
Text HLabel 11100 2950 2    60   Input ~ 0
P1.15_ENTHrefCLK_88
Text HLabel 11100 2800 2    60   Input ~ 0
P1.14_ENTH_rxER_89
Text HLabel 11100 2650 2    60   Input ~ 0
P1.10_ENTHrxd1_90
Text HLabel 11050 2500 2    60   Input ~ 0
P1.9_ENTHrxd0_91
Text HLabel 11300 3900 2    60   Input ~ 0
P1.20_PWM1.2_QEI(A)_SCK0_34
Text Label 10150 3900 0    60   ~ 0
P1.20_PWM1.2_QEI(A)_SCK0_34
Text HLabel 11300 4050 2    60   Input ~ 0
P1.21_PWM1.3_SSEL0_35
Text Label 10150 4050 0    60   ~ 0
P1.21_PWM1.3_SSEL0_35
Text HLabel 11250 4350 2    60   Input ~ 0
P1.23_MISO0_QEI(B)_PWM1.4_37
Text Label 10150 4350 0    60   ~ 0
P1.23_MISO0_QEI(B)_PWM1.4_37
Text HLabel 11250 4500 2    60   Input ~ 0
P1.24_MOSI0_PWM1.5_QEI(IDX)_38
Text Label 10150 4500 0    60   ~ 0
P1.24_MOSI0_PWM1.5_QEI(IDX)_38
Text HLabel 11250 4650 2    60   Input ~ 0
P1.25_MAT1.1_39
Text Label 10150 4650 0    60   ~ 0
P1.25_MAT1.1_39
Text HLabel 11250 4800 2    60   Input ~ 0
P1.26_PWM1.6_40
Text Label 10250 4800 0    60   ~ 0
P1.26_PWM1.6_40
Text HLabel 11250 5100 2    60   Input ~ 0
P1.28_PCAP1.0_44
Text Label 10200 5100 0    60   ~ 0
P1.28_PCAP1.0_44
Text HLabel 11250 5250 2    60   Input ~ 0
P1.29_PCAP1.1_45
Text Label 10200 5250 0    60   ~ 0
P1.29_PCAP1.1_45
Text HLabel 11250 5850 2    60   Input ~ 0
P2.0_PWM1.1_TXD1_75
Text Label 10150 5850 0    60   ~ 0
P2.0_PWM1.1_TXD1_75
Text HLabel 11250 6000 2    60   Input ~ 0
P2.1_PWM1.2_RXD1_74
Text Label 10150 6000 0    60   ~ 0
P2.1_PWM1.2_RXD1_74
Text HLabel 11250 6150 2    60   Input ~ 0
P2.2_PWM1.3_73
Text HLabel 11250 6300 2    60   Input ~ 0
P2.3_PWM1.4_70
Text HLabel 11250 6450 2    60   Input ~ 0
P2.4_PWM1.5_69
Text HLabel 11250 6600 2    60   Input ~ 0
P2.5_PWM1.6_68
Text HLabel 11250 6750 2    60   Input ~ 0
P2.6_67
Text HLabel 11250 6900 2    60   Input ~ 0
P2.7_RTS1_66
Text Label 10200 6150 0    60   ~ 0
P2.2_PWM1.3_73
Text Label 10200 6300 0    60   ~ 0
P2.3_PWM1.4_70
Text Label 10200 6450 0    60   ~ 0
P2.4_PWM1.5_69
Text Label 10200 6600 0    60   ~ 0
P2.5_PWM1.6_68
Text Label 10200 6750 0    60   ~ 0
P2.6_67
Text Label 10200 6900 0    60   ~ 0
P2.7_RTS1_66
Text HLabel 11250 7050 2    60   Input ~ 0
P2.8_TXD2_65
Text HLabel 11250 7200 2    60   Input ~ 0
P2.9_RXD2_USBconnect_64
Text HLabel 11250 7350 2    60   Input ~ 0
P2.10_EINT0_NMI_53
Text HLabel 11250 7500 2    60   Input ~ 0
P2.11_EINT1_52
Text HLabel 11250 7650 2    60   Input ~ 0
P2.12_EINT2_51
Text HLabel 11250 7800 2    60   Input ~ 0
P2.13_EINT3_50
Text Label 10200 7050 0    60   ~ 0
P2.8_TXD2_65
Text Label 10200 7200 0    60   ~ 0
P2.9_RXD2_64
Text Label 10200 7350 0    60   ~ 0
P2.10_EINT0_NMI_53
Text Label 10200 7500 0    60   ~ 0
P2.11_EINT1_52
Text Label 10200 7650 0    60   ~ 0
P2.12_EINT2_51
Text Label 10200 7800 0    60   ~ 0
P2.13_EINT3_50
Text HLabel 10900 8100 2    60   Input ~ 0
TDO_SWO_1
Text Label 10250 8100 0    60   ~ 0
TDO_SWO_1
Text Label 10250 8250 0    60   ~ 0
TDI_2
Text HLabel 10900 8250 2    60   Input ~ 0
TDI_2
Text HLabel 10900 8400 2    60   Input ~ 0
TMS_SWDIO_3
Text Label 10250 8400 0    60   ~ 0
TMS_SWDIO_3
Text HLabel 10900 8550 2    60   Input ~ 0
TRST_4
Text Label 10250 8550 0    60   ~ 0
TRST_4
Text HLabel 10900 8700 2    60   Input ~ 0
TCK_SWDCLK_5
Text Label 10250 8700 0    60   ~ 0
TCK_SWDCLK_5
Text HLabel 10900 8850 2    60   Input ~ 0
RTCK_100
Text Label 10250 8850 0    60   ~ 0
RTCK_100
Text HLabel 10900 9000 2    60   Input ~ 0
RSTOUT_14
Text HLabel 10900 9150 2    60   Input ~ 0
RESET_17
Text Label 10250 9000 0    60   ~ 0
RSTOUT_14
Text Label 10250 9150 0    60   ~ 0
RESET_17
$Comp
L C C43
U 1 1 5269C4A5
P 11250 9500
F 0 "C43" H 11250 9600 40  0000 L CNN
F 1 "22pF" H 11256 9415 40  0000 L CNN
F 2 "AutomataFootprint:C_0805_AUTOMATA" H 11288 9350 30  0001 C CNN
F 3 "~" H 11250 9500 60  0000 C CNN
	1    11250 9500
	0    -1   -1   0   
$EndComp
$Comp
L C C44
U 1 1 5269C4B2
P 11250 10100
F 0 "C44" H 11250 10200 40  0000 L CNN
F 1 "22pF" H 11256 10015 40  0000 L CNN
F 2 "AutomataFootprint:C_0805_AUTOMATA" H 11288 9950 30  0001 C CNN
F 3 "~" H 11250 10100 60  0000 C CNN
	1    11250 10100
	0    -1   -1   0   
$EndComp
Text Label 11600 9500 0    60   ~ 0
Processor_GND
Text Label 2450 8800 0    60   ~ 0
Processor_GND
Text Label 2450 8400 0    60   ~ 0
3.3_V
Text HLabel 5850 10200 0    60   Input ~ 0
V_BAT_19
Text Label 6100 10200 2    60   ~ 0
V_BAT
Text HLabel 6050 9850 0    60   Input ~ 0
ADC_VREFp_12
Text Label 6050 8050 2    60   ~ 0
VDD_A
Text HLabel 6050 9950 0    60   Input ~ 0
ADC_VREFn_15
Text HLabel 5750 8050 0    60   Input ~ 0
ADC_VDD_A_10
Text HLabel 5500 8250 0    60   Input ~ 0
ADC_VSS_A_11
$Comp
L Crystal Y2
U 1 1 58F8534F
P 10900 9800
F 0 "Y2" H 10900 9950 50  0000 C CNN
F 1 "12MHZ" H 10900 9650 50  0000 C CNN
F 2 "AutomataFootprint:Crystal_HC18-U_Vertical" H 10900 9800 50  0001 C CNN
F 3 "" H 10900 9800 50  0001 C CNN
	1    10900 9800
	0    1    1    0   
$EndComp
Wire Wire Line
	1600 2550 1850 2550
Wire Wire Line
	1600 2700 1850 2700
Wire Wire Line
	6050 9200 6200 9200
Connection ~ 6200 9300
Connection ~ 6200 9400
Connection ~ 6200 9500
Wire Wire Line
	10050 3600 11250 3600
Wire Wire Line
	10050 3750 11250 3750
Wire Wire Line
	10050 4200 11250 4200
Wire Wire Line
	10050 4950 11250 4950
Wire Wire Line
	4950 4650 6200 4650
Wire Wire Line
	4950 4800 6200 4800
Wire Wire Line
	5100 5250 6200 5250
Wire Wire Line
	5150 5600 6200 5600
Wire Wire Line
	5150 5900 6200 5900
Wire Wire Line
	10050 5400 11250 5400
Wire Wire Line
	10050 5550 11250 5550
Connection ~ 6200 8350
Connection ~ 6200 8450
Connection ~ 6200 8550
Connection ~ 6200 8750
Connection ~ 6200 8650
Wire Wire Line
	5000 3850 6200 3850
Wire Wire Line
	4950 4200 6200 4200
Wire Wire Line
	4900 4350 6200 4350
Wire Wire Line
	4900 4500 6200 4500
Wire Wire Line
	4950 2200 6200 2200
Wire Wire Line
	4950 2350 6200 2350
Wire Wire Line
	5150 6050 6200 6050
Wire Wire Line
	5150 6200 6200 6200
Wire Wire Line
	5300 6350 6200 6350
Wire Wire Line
	5300 6500 6200 6500
Wire Wire Line
	5000 6850 6200 6850
Wire Wire Line
	5000 7000 6200 7000
Wire Wire Line
	5300 7350 6200 7350
Wire Wire Line
	5000 7500 6200 7500
Wire Wire Line
	6000 7750 6200 7750
Wire Wire Line
	6200 7850 6100 7850
Wire Wire Line
	6100 7850 6100 7750
Connection ~ 6100 7750
Wire Wire Line
	4950 2050 6200 2050
Wire Wire Line
	4950 1900 6200 1900
Wire Wire Line
	5250 2500 6200 2500
Wire Wire Line
	5250 2650 6200 2650
Wire Wire Line
	5350 2800 6200 2800
Wire Wire Line
	5350 2950 6200 2950
Wire Wire Line
	5350 3250 6200 3250
Wire Wire Line
	5350 3400 6200 3400
Wire Wire Line
	5050 3550 6200 3550
Wire Wire Line
	4950 3700 6200 3700
Wire Wire Line
	5650 4950 6200 4950
Wire Wire Line
	5650 5100 6200 5100
Wire Wire Line
	10050 1900 11000 1900
Wire Wire Line
	10050 2050 11000 2050
Wire Wire Line
	10050 2200 11000 2200
Wire Wire Line
	10050 2350 11000 2350
Wire Wire Line
	10050 2500 11050 2500
Wire Wire Line
	10050 2650 11100 2650
Wire Wire Line
	10050 2800 11100 2800
Wire Wire Line
	10050 2950 11100 2950
Wire Wire Line
	10050 3100 11100 3100
Wire Wire Line
	10050 3250 11100 3250
Wire Wire Line
	10050 3900 11300 3900
Wire Wire Line
	10050 4050 11300 4050
Wire Wire Line
	10050 4350 11250 4350
Wire Wire Line
	10050 4500 11250 4500
Wire Wire Line
	10050 4650 11250 4650
Wire Wire Line
	10050 4800 11250 4800
Wire Wire Line
	10050 5100 11250 5100
Wire Wire Line
	10050 5250 11250 5250
Wire Wire Line
	10050 5850 11250 5850
Wire Wire Line
	10050 6000 11250 6000
Wire Wire Line
	10050 6150 11250 6150
Wire Wire Line
	10050 6300 11250 6300
Wire Wire Line
	10050 6450 11250 6450
Wire Wire Line
	10050 6600 11250 6600
Wire Wire Line
	10050 6750 11250 6750
Wire Wire Line
	10050 6900 11250 6900
Wire Wire Line
	10050 7050 11250 7050
Wire Wire Line
	10050 7200 11250 7200
Wire Wire Line
	10050 7350 11250 7350
Wire Wire Line
	10050 7500 11250 7500
Wire Wire Line
	10050 7650 11250 7650
Wire Wire Line
	10050 7800 11250 7800
Wire Wire Line
	10050 8100 10900 8100
Wire Wire Line
	10050 8250 10900 8250
Wire Wire Line
	10050 8400 10900 8400
Wire Wire Line
	10050 8550 10900 8550
Wire Wire Line
	10050 8700 10900 8700
Wire Wire Line
	10050 8850 10900 8850
Wire Wire Line
	10050 9000 10900 9000
Wire Wire Line
	10050 9150 10900 9150
Wire Wire Line
	10050 9550 10750 9550
Wire Wire Line
	10750 9550 10750 9500
Wire Wire Line
	10750 9500 11100 9500
Connection ~ 10900 9500
Wire Wire Line
	11450 9500 11450 10100
Wire Wire Line
	11400 9500 11600 9500
Wire Wire Line
	10050 9850 10150 9850
Wire Wire Line
	10050 10000 10150 10000
Wire Wire Line
	650  8400 2450 8400
Wire Wire Line
	650  8800 2450 8800
Wire Wire Line
	5150 5750 6200 5750
Wire Wire Line
	5850 10200 6200 10200
Connection ~ 6200 9600
Wire Wire Line
	6050 9950 6200 9950
Wire Wire Line
	6200 9200 6200 9600
Wire Wire Line
	6050 9850 6200 9850
Wire Wire Line
	5750 8050 6200 8050
Connection ~ 1000 8400
Connection ~ 1300 8400
Connection ~ 1300 8800
Connection ~ 1000 8800
Wire Wire Line
	6050 8350 6200 8350
Wire Wire Line
	6200 8350 6200 8850
Wire Wire Line
	5500 8250 6200 8250
Wire Wire Line
	10050 9700 10700 9700
Wire Wire Line
	10700 9700 10700 10150
Wire Wire Line
	10700 10150 11050 10150
Wire Wire Line
	11050 10150 11050 10100
Wire Wire Line
	10900 9950 10900 10150
Connection ~ 10900 10150
Wire Wire Line
	10900 9500 10900 9650
Wire Wire Line
	11050 10100 11100 10100
Wire Wire Line
	11450 10100 11400 10100
Connection ~ 11450 9500
Wire Wire Line
	650  8400 650  8450
Wire Wire Line
	1000 8400 1000 8450
Wire Wire Line
	1300 8450 1300 8400
Wire Wire Line
	650  8800 650  8750
Wire Wire Line
	1000 8750 1000 8800
Wire Wire Line
	1300 8750 1300 8800
$Comp
L LPC1768FBD100 IC1
U 1 1 52569F3A
P 8650 3350
F 0 "IC1" H 6650 5050 60  0000 C CNN
F 1 "LPC1769FBD100" H 8300 3850 60  0000 C CNN
F 2 "AutomataFootprint:LQFP100_0.5mm_SOT407-1_LPC1768" H 8650 3350 60  0001 C CNN
F 3 "~" H 8650 3350 60  0000 C CNN
	1    8650 3350
	1    0    0    -1  
$EndComp
$Comp
L C C40
U 1 1 58FF35AF
P 650 8600
F 0 "C40" H 650 8700 40  0000 L CNN
F 1 "100nF" H 656 8515 40  0000 L CNN
F 2 "AutomataFootprint:C_0805_AUTOMATA" H 688 8450 30  0001 C CNN
F 3 "~" H 650 8600 60  0000 C CNN
	1    650  8600
	1    0    0    -1  
$EndComp
$Comp
L C C41
U 1 1 58FF3A09
P 1000 8600
F 0 "C41" H 1000 8700 40  0000 L CNN
F 1 "100nF" H 1006 8515 40  0000 L CNN
F 2 "AutomataFootprint:C_0805_AUTOMATA" H 1038 8450 30  0001 C CNN
F 3 "~" H 1000 8600 60  0000 C CNN
	1    1000 8600
	1    0    0    -1  
$EndComp
$Comp
L C C42
U 1 1 58FF3E5A
P 1300 8600
F 0 "C42" H 1300 8700 40  0000 L CNN
F 1 "100nF" H 1306 8515 40  0000 L CNN
F 2 "AutomataFootprint:C_0805_AUTOMATA" H 1338 8450 30  0001 C CNN
F 3 "~" H 1300 8600 60  0000 C CNN
	1    1300 8600
	1    0    0    -1  
$EndComp
$Comp
L C C48
U 1 1 5904DBAF
P 1550 8600
F 0 "C48" H 1550 8700 40  0000 L CNN
F 1 "100nF" H 1556 8515 40  0000 L CNN
F 2 "AutomataFootprint:C_0805_AUTOMATA" H 1588 8450 30  0001 C CNN
F 3 "~" H 1550 8600 60  0000 C CNN
	1    1550 8600
	1    0    0    -1  
$EndComp
$Comp
L C C49
U 1 1 5904DBED
P 1800 8600
F 0 "C49" H 1800 8700 40  0000 L CNN
F 1 "100nF" H 1806 8515 40  0000 L CNN
F 2 "AutomataFootprint:C_0805_AUTOMATA" H 1838 8450 30  0001 C CNN
F 3 "~" H 1800 8600 60  0000 C CNN
	1    1800 8600
	1    0    0    -1  
$EndComp
$Comp
L C C50
U 1 1 5904DC26
P 2100 8600
F 0 "C50" H 2100 8700 40  0000 L CNN
F 1 "100nF" H 2106 8515 40  0000 L CNN
F 2 "AutomataFootprint:C_0805_AUTOMATA" H 2138 8450 30  0001 C CNN
F 3 "~" H 2100 8600 60  0000 C CNN
	1    2100 8600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 8450 1550 8400
Connection ~ 1550 8400
Wire Wire Line
	1550 8750 1550 8800
Connection ~ 1550 8800
Wire Wire Line
	1800 8750 1800 8800
Connection ~ 1800 8800
Wire Wire Line
	1800 8450 1800 8400
Connection ~ 1800 8400
Wire Wire Line
	2100 8450 2100 8400
Connection ~ 2100 8400
Wire Wire Line
	2100 8750 2100 8800
Connection ~ 2100 8800
Text Notes 1150 9000 0    60   ~ 0
Isolation capacitors
Text Notes 10900 10400 0    60   ~ 0
Oscillator circuit
Text Label 10900 9500 2    60   ~ 0
XTAL1
Text Label 10900 10150 2    60   ~ 0
XTAL2
$Comp
L C C2
U 1 1 5931DB8E
P 14450 6850
F 0 "C2" H 14450 6950 40  0000 L CNN
F 1 "22pF" H 14456 6765 40  0000 L CNN
F 2 "AutomataFootprint:C_0805_AUTOMATA" H 14488 6700 30  0001 C CNN
F 3 "~" H 14450 6850 60  0000 C CNN
	1    14450 6850
	0    -1   -1   0   
$EndComp
$Comp
L C C3
U 1 1 5931DB94
P 14450 7450
F 0 "C3" H 14450 7550 40  0000 L CNN
F 1 "22pF" H 14456 7365 40  0000 L CNN
F 2 "AutomataFootprint:C_0805_AUTOMATA" H 14488 7300 30  0001 C CNN
F 3 "~" H 14450 7450 60  0000 C CNN
	1    14450 7450
	0    -1   -1   0   
$EndComp
Text Label 14800 6850 0    60   ~ 0
Processor_GND
$Comp
L Crystal Y3
U 1 1 5931DB9D
P 14100 7150
F 0 "Y3" H 14100 7300 50  0000 C CNN
F 1 "32.768KHZ" H 14100 7000 50  0000 C CNN
F 2 "AutomataFootprint1:Crystal_Cylinder_1.1mm_Spacing" H 14100 7150 50  0001 C CNN
F 3 "" H 14100 7150 50  0001 C CNN
	1    14100 7150
	0    1    1    0   
$EndComp
Wire Wire Line
	13250 6900 13950 6900
Wire Wire Line
	13950 6900 13950 6850
Wire Wire Line
	13950 6850 14300 6850
Connection ~ 14100 6850
Wire Wire Line
	14650 6850 14650 7450
Wire Wire Line
	14600 6850 14800 6850
Wire Wire Line
	13250 7050 13900 7050
Wire Wire Line
	13900 7050 13900 7500
Wire Wire Line
	13900 7500 14250 7500
Wire Wire Line
	14250 7500 14250 7450
Wire Wire Line
	14100 7300 14100 7500
Connection ~ 14100 7500
Wire Wire Line
	14100 6850 14100 7000
Wire Wire Line
	14250 7450 14300 7450
Wire Wire Line
	14650 7450 14600 7450
Connection ~ 14650 6850
Text Notes 14100 7750 0    60   ~ 0
Oscillator circuit
Text Label 13250 6900 2    60   ~ 0
RTCX1
Text Label 13250 7050 2    60   ~ 0
RTCX2
Text Label 10150 9850 0    60   ~ 0
RTCX1
Text Label 10150 10000 0    60   ~ 0
RTCX2
Text Notes 7100 1300 0    394  ~ 0
LPC1769
$EndSCHEMATC
