EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:automata
LIBS:automata_pwm
LIBS:CNCInterface-cache
LIBS:opendous
LIBS:FALCON_1769-cache
LIBS:FALCON_1769-rescue
LIBS:KEVIN-rescue
LIBS:AUTOPLCV1.4-cache
EELAYER 25 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 10 11
Title "RDM_lpc2148"
Date "15 oct 2016"
Rev "0.5"
Comp "AUTOMATA SYSTEMS"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 2450 1600 0    60   ~ 0
5.08MM PITCH \nScrew Terminal
Text Notes 700  350  0    60   ~ 0
TOP PCB
$Comp
L CP1 C6
U 1 1 4CA18EC6
P 7600 1700
F 0 "C6" H 7650 1800 50  0000 L CNN
F 1 "330uF/50V" H 7650 1600 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D12.5mm_P7.50mm" H 7600 1700 60  0001 C CNN
F 3 "" H 7600 1700 60  0001 C CNN
	1    7600 1700
	1    0    0    -1  
$EndComp
$Comp
L CP1 C5
U 1 1 4CA18DFB
P 3800 1650
F 0 "C5" H 3850 1750 50  0000 L CNN
F 1 "100uF/50V" H 3850 1550 50  0000 L CNN
F 2 "AutomataFootprint:CP_Radial_D10.0mm_P5.00mm" H 3800 1650 60  0001 C CNN
F 3 "" H 3800 1650 60  0001 C CNN
	1    3800 1650
	1    0    0    -1  
$EndComp
Text HLabel 3150 1300 0    60   Input ~ 0
12V
Text HLabel 3150 2000 0    60   Input ~ 0
GND_SUPPLY
Text HLabel 8200 1100 2    60   Input ~ 0
5V
Wire Wire Line
	3150 1300 5750 1300
Wire Wire Line
	3800 1300 3800 1500
Connection ~ 3800 1300
Wire Wire Line
	3800 1800 3800 2000
Connection ~ 3800 2000
Wire Wire Line
	7600 1100 7600 1550
Wire Wire Line
	7600 2000 7600 1850
Wire Wire Line
	3150 2000 7600 2000
$Comp
L Conn_01x03 J1
U 1 1 5A54DFA0
P 5850 900
F 0 "J1" H 5850 1100 50  0000 C CNN
F 1 "PWR_MODULE" V 6000 900 50  0000 C CNN
F 2 "AutomataFootprint:TO220_VERT_automata" H 5850 900 50  0001 C CNN
F 3 "" H 5850 900 50  0001 C CNN
	1    5850 900 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5750 1300 5750 1100
Wire Wire Line
	5850 1100 5850 2000
Connection ~ 5850 2000
Wire Wire Line
	5950 1100 8200 1100
Connection ~ 7600 1100
Text HLabel 12050 3800 1    60   Input ~ 0
3.3V
Text Notes 7700 4260 2    60   ~ 0
Power On Led\n
Text HLabel 6500 4050 0    60   Input ~ 0
5v
Text Notes 7750 3860 2    60   ~ 0
Power On Led\n
Text HLabel 6550 3650 0    60   Input ~ 0
12V
Text Notes 7650 4710 2    60   ~ 0
Power On Led\n
Text HLabel 6500 4500 0    60   Input ~ 0
3.3V
Text HLabel 7850 3650 2    60   Input ~ 0
GND_SUPPLY
Text HLabel 7800 4050 2    60   Input ~ 0
GND_SUPPLY
Text HLabel 7800 4500 2    60   Input ~ 0
GND_SUPPLY
Text HLabel 11850 4500 2    60   Input ~ 0
GND_SUPPLY
Wire Wire Line
	7000 4050 7300 4050
Wire Wire Line
	7600 4050 7800 4050
Wire Wire Line
	6500 4050 6700 4050
Wire Wire Line
	7050 3650 7450 3650
Wire Wire Line
	7750 3650 7850 3650
Wire Wire Line
	6550 3650 6750 3650
Wire Wire Line
	7000 4500 7300 4500
Wire Wire Line
	7600 4500 7800 4500
Wire Wire Line
	6500 4500 6700 4500
Wire Wire Line
	11850 4550 11850 4400
Wire Wire Line
	10800 4000 10600 4000
Wire Wire Line
	11200 4550 11850 4550
Connection ~ 11850 4500
Wire Wire Line
	12050 4000 12050 3800
$Comp
L LED D4
U 1 1 5A96E4CF
P 6900 3650
F 0 "D4" H 6900 3750 50  0000 C CNN
F 1 "12V" H 6900 3550 50  0000 C CNN
F 2 "AutomataFootprint:LED_1206" H 6900 3650 50  0001 C CNN
F 3 "" H 6900 3650 50  0001 C CNN
	1    6900 3650
	-1   0    0    1   
$EndComp
$Comp
L LED D2
U 1 1 5A96E4D6
P 6850 4050
F 0 "D2" H 6850 4150 50  0000 C CNN
F 1 "5V" H 6850 3950 50  0000 C CNN
F 2 "AutomataFootprint:LED_1206" H 6850 4050 50  0001 C CNN
F 3 "" H 6850 4050 50  0001 C CNN
	1    6850 4050
	-1   0    0    1   
$EndComp
$Comp
L LED D3
U 1 1 5A96E4DD
P 6850 4500
F 0 "D3" H 6850 4600 50  0000 C CNN
F 1 "3.3V" H 6850 4400 50  0000 C CNN
F 2 "AutomataFootprint:LED_1206" H 6850 4500 50  0001 C CNN
F 3 "" H 6850 4500 50  0001 C CNN
	1    6850 4500
	-1   0    0    1   
$EndComp
$Comp
L LM1117 U1
U 1 1 5A96E4E5
P 11200 4150
F 0 "U1" H 11200 4450 60  0000 C CNN
F 1 "LM1117" H 11250 3900 60  0000 L CNN
F 2 "AutomataFootprint:SOT223_LM1117" H 11200 4150 60  0001 C CNN
F 3 "" H 11200 4150 60  0000 C CNN
	1    11200 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	11600 4000 12050 4000
Wire Wire Line
	11600 4000 11600 4100
$Comp
L CP1 C7
U 1 1 5A96E4EE
P 11850 4250
F 0 "C7" H 11875 4350 50  0000 L CNN
F 1 "100uF" H 11875 4150 50  0000 L CNN
F 2 "AutomataFootprint:CP_Radial_D10.0mm_P5.00mm" H 11850 4250 50  0001 C CNN
F 3 "" H 11850 4250 50  0001 C CNN
	1    11850 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	11200 4550 11200 4500
Wire Wire Line
	11850 4100 11850 4000
Connection ~ 11850 4000
$Comp
L R R7
U 1 1 5A96E4F8
P 7600 3650
F 0 "R7" V 7680 3650 50  0000 C CNN
F 1 "2k" V 7600 3650 50  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMAT" V 7530 3650 50  0001 C CNN
F 3 "" H 7600 3650 50  0001 C CNN
	1    7600 3650
	0    1    1    0   
$EndComp
$Comp
L R R5
U 1 1 5A96E4FF
P 7450 4050
F 0 "R5" V 7530 4050 50  0000 C CNN
F 1 "1k" V 7450 4050 50  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMAT" V 7380 4050 50  0001 C CNN
F 3 "" H 7450 4050 50  0001 C CNN
	1    7450 4050
	0    1    1    0   
$EndComp
$Comp
L R R6
U 1 1 5A96E506
P 7450 4500
F 0 "R6" V 7530 4500 50  0000 C CNN
F 1 "560E" V 7450 4500 50  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMAT" V 7380 4500 50  0001 C CNN
F 3 "" H 7450 4500 50  0001 C CNN
	1    7450 4500
	0    1    1    0   
$EndComp
Wire Notes Line
	5700 3400 8900 3400
Wire Notes Line
	8900 3400 8900 5150
Wire Notes Line
	8900 5150 5700 5150
Wire Notes Line
	5700 5150 5700 3400
Wire Notes Line
	10000 3350 12600 3350
Wire Notes Line
	12600 3350 12600 5150
Wire Notes Line
	12600 5150 10000 5150
Wire Notes Line
	10000 5150 10000 3350
Text Notes 6850 3350 0    197  ~ 0
INDICATORS
Text Notes 11200 3250 0    197  ~ 0
LM1117
$Comp
L Conn_01x02 J16
U 1 1 5A971461
P 2900 3850
F 0 "J16" H 2900 3950 50  0000 C CNN
F 1 "POWER" V 3000 3850 50  0000 C CNN
F 2 "AutomataFootprint:PHX_2PIN_5.08_2.8_SPACING" H 2900 3850 50  0001 C CNN
F 3 "" H 2900 3850 50  0001 C CNN
	1    2900 3850
	-1   0    0    1   
$EndComp
Wire Wire Line
	3100 3750 3600 3750
Wire Wire Line
	3600 3750 3600 3600
Wire Wire Line
	3100 3850 3600 3850
Wire Wire Line
	3600 3850 3600 4000
Wire Wire Line
	3600 4000 3900 4000
Wire Wire Line
	3250 3850 3250 4200
Wire Wire Line
	3250 4200 3100 4200
Connection ~ 3250 3850
Text HLabel 3100 4200 0    79   Input ~ 0
EXT_24V
$Comp
L D D68
U 1 1 5A971471
P 4050 4000
F 0 "D68" H 4050 4100 50  0000 C CNN
F 1 "D" H 4050 3900 50  0000 C CNN
F 2 "AutomataFootprint:D_SMC_Standard_Automata" H 4050 4000 50  0001 C CNN
F 3 "" H 4050 4000 50  0001 C CNN
	1    4050 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	4200 4000 4300 4000
Text HLabel 4300 4000 2    60   Input ~ 0
12V
Text HLabel 3600 3600 2    60   Input ~ 0
GND_SUPPLY
Text Label 6600 4050 2    39   ~ 0
5V
Text HLabel 10600 4000 0    60   Input ~ 0
5v
$EndSCHEMATC
