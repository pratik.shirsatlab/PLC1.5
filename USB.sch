EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:AUTOPLCV1.4-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 10
Title ""
Date "29 jul 2017"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 600  650  0    40   ~ 0
USB-A type Connector\nJumper on the VBUS line, to be connected only if the power source is USB, \nelse donot put the jumper\n
Text HLabel 2500 4350 0    60   Input ~ 0
5v_VCC
Text Label 5300 4550 0    60   ~ 0
USB_5V
Text Label 4650 4200 2    60   ~ 0
USB_DP
Text Label 4950 4350 0    60   ~ 0
USB_DM
Text Label 4050 4250 2    60   ~ 0
DP
Text Label 3200 3350 3    60   ~ 0
USB_5V
Text Label 6650 5500 0    60   ~ 0
USB_UPLED
Text HLabel 2450 4600 0    40   Input ~ 0
USB_DP
Text HLabel 2400 4700 0    40   Input ~ 0
USB_DM
Text Label 2600 4600 0    60   ~ 0
USB_DP
Text Label 2550 4700 0    60   ~ 0
USB_DM
Text Label 2650 4900 0    60   ~ 0
USB_UPLED
Text HLabel 2450 4900 0    40   Input ~ 0
USB_UP_LED
Text Label 4100 4650 2    60   ~ 0
DN
Text Notes 5500 2800 0    60   ~ 0
this 5volt supply should be \nisolated from board 5volts
Text HLabel 2400 5100 0    40   Input ~ 0
USB_CONNECT
Text Label 2600 5100 0    60   ~ 0
USB_CONNECT
Text Label 2950 6150 2    60   ~ 0
USB_CONNECT
$Comp
L BC856 Q2
U 1 1 540764E4
P 4100 6150
F 0 "Q2" H 4100 6001 40  0000 R CNN
F 1 "BC856" H 4100 6300 40  0000 R CNN
F 2 "AutomataFootprint:TO92" H 4000 6252 29  0001 C CNN
F 3 "~" H 4100 6150 60  0000 C CNN
	1    4100 6150
	1    0    0    -1  
$EndComp
Text Label 3350 6550 2    60   ~ 0
USB_DP
Text HLabel 2500 4100 0    60   Input ~ 0
USB_3V3
Text Label 2650 4100 0    60   ~ 0
3.3_VOLT
Text Label 3550 5500 2    60   ~ 0
3.3_VOLT
Text Label 5750 5450 0    60   ~ 0
VBUS
Text Label 2650 5300 0    60   ~ 0
VBUS
Text HLabel 2400 5300 0    40   Input ~ 0
USB_VBUS
Text Label 2650 4350 0    60   ~ 0
BORD_5V
Text Label 2600 3350 3    60   ~ 0
BORD_5V
$Comp
L C-RESCUE-PACMAC_LPC_1769 C31
U 1 1 5577CF6F
P 5300 5100
F 0 "C31" H 5300 5200 40  0000 L CNN
F 1 "100nF" H 5306 5015 40  0000 L CNN
F 2 "AutomataFootprint:AutomataFootprint_C_0805_AUTOMATA" H 5338 4950 30  0000 C CNN
F 3 "~" H 5300 5100 60  0000 C CNN
	1    5300 5100
	1    0    0    -1  
$EndComp
$Comp
L JUMPER JP2
U 1 1 5577CF95
P 2900 3200
F 0 "JP2" H 2900 3350 60  0000 C CNN
F 1 "USB_SUPPLY" H 2900 3120 40  0000 C CNN
F 2 "AutomataFootprint:Automata_Pin_Header_Straight_2x01_Pitch2.54mm" H 2900 3200 60  0001 C CNN
F 3 "~" H 2900 3200 60  0000 C CNN
	1    2900 3200
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-PACMAC_LPC_1769 R28
U 1 1 5577D6B5
P 4600 4650
F 0 "R28" V 4680 4650 40  0000 C CNN
F 1 "33" V 4607 4651 40  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMAT" V 4530 4650 30  0001 C CNN
F 3 "~" H 4600 4650 30  0000 C CNN
	1    4600 4650
	0    -1   -1   0   
$EndComp
$Comp
L R-RESCUE-PACMAC_LPC_1769 R29
U 1 1 5577D6C5
P 6650 5000
F 0 "R29" V 6730 5000 40  0000 C CNN
F 1 "470R" V 6657 5001 40  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMAT" V 6580 5000 30  0001 C CNN
F 3 "~" H 6650 5000 30  0000 C CNN
	1    6650 5000
	-1   0    0    1   
$EndComp
$Comp
L R-RESCUE-PACMAC_LPC_1769 R33
U 1 1 5577DA5D
P 5050 5100
F 0 "R33" V 5130 5100 40  0000 C CNN
F 1 "100k" V 5057 5101 40  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMAT" V 4980 5100 30  0001 C CNN
F 3 "~" H 5050 5100 30  0000 C CNN
	1    5050 5100
	-1   0    0    1   
$EndComp
$Comp
L R-RESCUE-PACMAC_LPC_1769 R30
U 1 1 5577DA6D
P 3350 6150
F 0 "R30" V 3430 6150 40  0000 C CNN
F 1 "2K" V 3357 6151 40  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMAT" V 3280 6150 30  0001 C CNN
F 3 "~" H 3350 6150 30  0000 C CNN
	1    3350 6150
	0    -1   -1   0   
$EndComp
$Comp
L R-RESCUE-PACMAC_LPC_1769 R31
U 1 1 5577DA73
P 3700 5850
F 0 "R31" V 3780 5850 40  0000 C CNN
F 1 "10K" V 3707 5851 40  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMAT" V 3630 5850 30  0001 C CNN
F 3 "~" H 3700 5850 30  0000 C CNN
	1    3700 5850
	-1   0    0    1   
$EndComp
$Comp
L R-RESCUE-PACMAC_LPC_1769 R32
U 1 1 5577DA8D
P 3750 6550
F 0 "R32" V 3830 6550 40  0000 C CNN
F 1 "1k5" V 3757 6551 40  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMAT" V 3680 6550 30  0001 C CNN
F 3 "~" H 3750 6550 30  0000 C CNN
	1    3750 6550
	0    -1   -1   0   
$EndComp
$Comp
L R-RESCUE-PACMAC_LPC_1769 R34
U 1 1 5577DE25
P 5200 6550
F 0 "R34" V 5280 6550 40  0000 C CNN
F 1 "470E" V 5207 6551 40  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMAT" V 5130 6550 30  0001 C CNN
F 3 "~" H 5200 6550 30  0000 C CNN
	1    5200 6550
	0    -1   -1   0   
$EndComp
$Comp
L R-RESCUE-PACMAC_LPC_1769 R35
U 1 1 5577DE35
P 5700 5100
F 0 "R35" V 5780 5100 40  0000 C CNN
F 1 "10K" V 5707 5101 40  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMAT" V 5630 5100 30  0001 C CNN
F 3 "~" H 5700 5100 30  0000 C CNN
	1    5700 5100
	-1   0    0    1   
$EndComp
$Comp
L R-RESCUE-PACMAC_LPC_1769 R36
U 1 1 5577E1CD
P 4400 4350
F 0 "R36" V 4480 4350 40  0000 C CNN
F 1 "33" V 4407 4351 40  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMAT" V 4330 4350 30  0001 C CNN
F 3 "~" H 4400 4350 30  0000 C CNN
	1    4400 4350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2500 4350 2650 4350
Wire Wire Line
	5300 4850 5300 4900
Wire Wire Line
	5050 5350 5300 5350
Wire Wire Line
	5300 5350 5300 5300
Wire Wire Line
	5050 4550 5050 4850
Wire Wire Line
	5050 4550 5300 4550
Wire Wire Line
	4650 4350 4650 4200
Wire Wire Line
	4850 4650 4950 4650
Wire Wire Line
	4950 4650 4950 4350
Wire Wire Line
	5100 5450 5100 5350
Connection ~ 5100 5350
Connection ~ 5050 4750
Wire Wire Line
	5050 4850 5700 4850
Wire Wire Line
	6650 4150 6650 4250
Wire Wire Line
	6650 4550 6650 4750
Wire Wire Line
	6650 5250 6650 5500
Wire Wire Line
	2450 4600 2600 4600
Wire Wire Line
	2400 4700 2550 4700
Wire Wire Line
	2450 4900 2650 4900
Wire Wire Line
	2600 5100 2400 5100
Wire Wire Line
	3100 6150 2950 6150
Wire Wire Line
	4000 6550 4400 6550
Wire Wire Line
	4200 6350 4200 6550
Connection ~ 4200 6550
Wire Wire Line
	4700 6550 4950 6550
Wire Wire Line
	5450 6550 5600 6550
Wire Wire Line
	5600 6550 5600 6650
Wire Wire Line
	3600 6150 3900 6150
Wire Wire Line
	3700 6100 3700 6150
Connection ~ 3700 6150
Wire Wire Line
	2650 4100 2500 4100
Wire Wire Line
	3550 5500 4200 5500
Wire Wire Line
	3700 5500 3700 5600
Wire Wire Line
	3350 6550 3500 6550
Wire Wire Line
	4200 5500 4200 5950
Connection ~ 3700 5500
Connection ~ 5300 4850
Wire Wire Line
	5750 5450 5700 5450
Wire Wire Line
	5700 5450 5700 5350
Wire Wire Line
	2400 5300 2650 5300
Wire Wire Line
	2600 3350 2600 3200
Wire Wire Line
	3200 3350 3200 3200
Wire Wire Line
	4350 4650 4100 4650
Wire Wire Line
	4050 4250 4050 4350
Wire Wire Line
	4050 4350 4150 4350
$Comp
L LED D7
U 1 1 5A81872C
P 6650 4400
F 0 "D7" H 6650 4500 50  0000 C CNN
F 1 "LED" H 6650 4300 50  0000 C CNN
F 2 "AutomataFootprint:LED_1206" H 6650 4400 50  0001 C CNN
F 3 "" H 6650 4400 50  0001 C CNN
	1    6650 4400
	0    -1   -1   0   
$EndComp
$Comp
L LED D6
U 1 1 5A818C0D
P 4550 6550
F 0 "D6" H 4550 6650 50  0000 C CNN
F 1 "LED" H 4550 6450 50  0000 C CNN
F 2 "AutomataFootprint:LED_1206" H 4550 6550 50  0001 C CNN
F 3 "" H 4550 6550 50  0001 C CNN
	1    4550 6550
	-1   0    0    1   
$EndComp
Text HLabel 2100 5500 0    59   Input ~ 0
GND
Wire Wire Line
	2100 5500 2350 5500
Text Label 2350 5500 0    59   ~ 0
GND
Text Label 5600 6650 0    59   ~ 0
GND
Text Label 6650 4150 0    60   ~ 0
3.3_VOLT
Text HLabel 2450 3950 0    79   Input ~ 0
USB_5V
Wire Wire Line
	2450 3950 2850 3950
Text Label 2850 3950 0    60   ~ 0
USB_5V
Text Label 5100 5450 2    59   ~ 0
GND
Text HLabel 2050 5700 0    60   Input ~ 0
DN
Text HLabel 2050 5850 0    60   Input ~ 0
DP
Text Label 2200 5850 0    60   ~ 0
DP
Wire Wire Line
	2200 5850 2050 5850
Wire Wire Line
	2050 5700 2200 5700
Text Label 2200 5700 0    60   ~ 0
DN
$EndSCHEMATC
