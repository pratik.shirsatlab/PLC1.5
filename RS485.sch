EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:automata
LIBS:automata_pwm
LIBS:CNCInterface-cache
LIBS:opendous
LIBS:FALCON_1769-cache
LIBS:FALCON_1769-rescue
LIBS:KEVIN-rescue
LIBS:AUTOPLCV1.4-cache
EELAYER 25 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 8 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R R26
U 1 1 5A7F2DFC
P 9500 5250
F 0 "R26" V 9580 5250 40  0000 C CNN
F 1 "620" V 9507 5251 40  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMATA" V 9430 5250 30  0001 C CNN
F 3 "~" H 9500 5250 30  0000 C CNN
	1    9500 5250
	1    0    0    -1  
$EndComp
Text Label 9500 4550 2    60   ~ 0
RX2-
Text Label 10300 4600 2    60   ~ 0
RX2+
$Comp
L R R27
U 1 1 5A7F2E05
P 10300 5300
F 0 "R27" V 10380 5300 40  0000 C CNN
F 1 "620" V 10307 5301 40  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMATA" V 10230 5300 30  0001 C CNN
F 3 "~" H 10300 5300 30  0000 C CNN
	1    10300 5300
	1    0    0    -1  
$EndComp
Text Label 10700 4600 2    60   ~ 0
TX2+
$Comp
L R R29
U 1 1 5A7F2E0D
P 11600 5300
F 0 "R29" V 11680 5300 40  0000 C CNN
F 1 "620" V 11607 5301 40  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMATA" V 11530 5300 30  0001 C CNN
F 3 "~" H 11600 5300 30  0000 C CNN
	1    11600 5300
	1    0    0    -1  
$EndComp
Text Label 11600 4600 2    60   ~ 0
TX2-
$Comp
L R R28
U 1 1 5A7F2E15
P 11200 5300
F 0 "R28" V 11280 5300 40  0000 C CNN
F 1 "620" V 11207 5301 40  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMATA" V 11130 5300 30  0001 C CNN
F 3 "~" H 11200 5300 30  0000 C CNN
	1    11200 5300
	1    0    0    -1  
$EndComp
Text Label 9500 3650 0    60   ~ 0
TX2+
Text Label 9500 3550 0    60   ~ 0
TX2-
$Comp
L SN75LBC184 U5
U 1 1 5A7F2E25
P 9050 3700
F 0 "U5" H 9050 3550 60  0000 C CNN
F 1 "SN75LBC184" H 9050 4050 60  0000 C CNN
F 2 "AutomataFootprint:SO8E" H 9000 3700 60  0001 C CNN
F 3 "~" H 9000 3700 60  0000 C CNN
	1    9050 3700
	1    0    0    -1  
$EndComp
$Comp
L LED D6
U 1 1 5A7F2E45
P 10700 3000
F 0 "D6" H 10700 3100 50  0000 C CNN
F 1 "LED" H 10700 2900 50  0000 C CNN
F 2 "AutomataFootprint:LED_0805_AUTOMATA" H 10700 3000 60  0001 C CNN
F 3 "~" H 10700 3000 60  0000 C CNN
	1    10700 3000
	0    1    1    0   
$EndComp
$Comp
L LED D7
U 1 1 5A7F2E4D
P 11400 3000
F 0 "D7" H 11400 3100 50  0000 C CNN
F 1 "LED" H 11400 2900 50  0000 C CNN
F 2 "AutomataFootprint:LED_0805_AUTOMATA" H 11400 3000 60  0001 C CNN
F 3 "~" H 11400 3000 60  0000 C CNN
	1    11400 3000
	0    1    1    0   
$EndComp
$Comp
L R R21
U 1 1 5A7F2E83
P 10700 3500
F 0 "R21" V 10780 3500 40  0000 C CNN
F 1 "390" V 10707 3501 40  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMATA" V 10630 3500 30  0001 C CNN
F 3 "~" H 10700 3500 30  0000 C CNN
	1    10700 3500
	1    0    0    -1  
$EndComp
$Comp
L R R22
U 1 1 5A7F2E8A
P 11400 3500
F 0 "R22" V 11480 3500 40  0000 C CNN
F 1 "390" V 11407 3501 40  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMATA" V 11330 3500 30  0001 C CNN
F 3 "~" H 11400 3500 30  0000 C CNN
	1    11400 3500
	1    0    0    -1  
$EndComp
Text HLabel 1500 2800 0    60   Input ~ 0
5V_VCC
Text HLabel 1500 2900 0    60   Input ~ 0
GND
Text Label 1650 2900 0    60   ~ 0
S_GND
Text Label 1650 2800 0    60   ~ 0
5V
Text Label 10250 5750 2    60   ~ 0
iso_5V
Text Label 11100 5750 2    60   ~ 0
iso_5V
Text Label 9450 5700 2    60   ~ 0
iso_gnd
Text Label 11500 5850 2    60   ~ 0
iso_gnd
Text Label 9650 3450 0    60   ~ 0
iso_5V
Text Label 9500 3750 0    60   ~ 0
iso_gnd
Text Notes 6850 1500 0    394  ~ 0
RS485\n
$Comp
L C C13
U 1 1 5A7F2F04
P 9500 3100
F 0 "C13" H 9525 3200 50  0000 L CNN
F 1 "0.1uF" H 9525 3000 50  0000 L CNN
F 2 "AutomataFootprint:C_0805_AUTOMATA" H 9538 2950 50  0001 C CNN
F 3 "" H 9500 3100 50  0001 C CNN
	1    9500 3100
	1    0    0    -1  
$EndComp
Text Label 9500 2850 0    60   ~ 0
iso_gnd
Text Label 8350 4050 3    60   ~ 0
ENABLE2
Text HLabel 1500 3200 0    60   Input ~ 0
ENABLE2
Text Label 1700 3200 0    60   ~ 0
ENABLE2
Wire Wire Line
	11400 2800 11400 2850
Wire Wire Line
	10700 2800 10700 2850
Connection ~ 11400 3800
Wire Wire Line
	11400 3650 11400 3800
Wire Wire Line
	10700 3650 10700 3800
Wire Wire Line
	10700 3800 11650 3800
Wire Wire Line
	9500 5700 9450 5700
Wire Wire Line
	11600 5850 11500 5850
Wire Wire Line
	10300 5750 10250 5750
Wire Wire Line
	11200 5750 11100 5750
Wire Wire Line
	11300 2800 11400 2800
Wire Wire Line
	10600 2800 10700 2800
Wire Wire Line
	1500 2900 1650 2900
Wire Wire Line
	1500 2800 1650 2800
Connection ~ 8350 3750
Wire Wire Line
	8350 3750 8350 4050
Wire Wire Line
	8600 3750 8650 3750
Wire Wire Line
	8600 3750 8600 3950
Wire Wire Line
	11300 2750 11300 2800
Wire Wire Line
	11400 3150 11400 3350
Wire Wire Line
	10600 2750 10600 2800
Wire Wire Line
	10700 3150 10700 3350
Wire Wire Line
	8550 3650 8650 3650
Wire Wire Line
	8550 3750 8550 3650
Wire Wire Line
	8150 3750 8550 3750
Wire Wire Line
	8450 3550 8650 3550
Wire Wire Line
	8450 3650 8450 3550
Wire Wire Line
	8150 3650 8450 3650
Wire Wire Line
	8150 3750 8150 3650
Wire Wire Line
	9450 3750 9500 3750
Wire Wire Line
	9450 3650 9500 3650
Wire Wire Line
	9450 3550 9500 3550
Wire Wire Line
	9450 3450 9650 3450
Wire Wire Line
	11200 5450 11200 5750
Wire Wire Line
	11200 4600 11200 5150
Wire Wire Line
	11600 5450 11600 5850
Wire Wire Line
	11600 4600 11600 5150
Wire Wire Line
	10700 4600 11200 4600
Wire Wire Line
	10300 5450 10300 5750
Wire Wire Line
	10300 4600 10300 5150
Wire Wire Line
	9500 5400 9500 5700
Wire Wire Line
	9500 4550 9500 5100
Wire Notes Line
	3850 2400 15700 2400
Wire Notes Line
	15700 2400 15700 6500
Wire Notes Line
	15700 6500 3850 6500
Wire Notes Line
	3850 6550 3850 2400
Connection ~ 9500 3450
Wire Wire Line
	9500 3450 9500 3250
Wire Wire Line
	9500 2850 9500 2950
Wire Wire Line
	1700 3200 1500 3200
$Comp
L CONN_3 K4
U 1 1 5A7F2FE0
P 5250 4650
F 0 "K4" V 5200 4650 50  0000 C CNN
F 1 "UART2" V 5300 4650 40  0000 C CNN
F 2 "AutomataFootprint:Pin_Header_Straight_1x03_Pitch2.54mm_Automata" H 5250 4650 60  0001 C CNN
F 3 "" H 5250 4650 60  0000 C CNN
	1    5250 4650
	1    0    0    -1  
$EndComp
Text Label 4900 4550 2    60   ~ 0
TX2
Text Label 4900 4650 2    60   ~ 0
RX2
Text Label 10600 2750 2    59   ~ 0
RX2
Text Label 11300 2750 2    59   ~ 0
TX2
Text Label 2400 3850 0    60   ~ 0
TX2+
Text Label 2400 4200 0    60   ~ 0
TX2-
Text Label 2400 3950 0    60   ~ 0
RX2+
Text Label 2400 4100 0    60   ~ 0
RX2-
Text HLabel 1900 3850 0    60   Input ~ 0
RS485_TX2+
Text HLabel 1950 4200 0    60   Input ~ 0
RS485_TX2-
Text HLabel 1900 3950 0    60   Input ~ 0
RS485_RX2+
Text HLabel 1900 4100 0    60   Input ~ 0
RS485_RX2-
Wire Wire Line
	2400 4100 1900 4100
Wire Wire Line
	2400 4200 1950 4200
Wire Wire Line
	1900 3950 2400 3950
Wire Wire Line
	1900 3850 2400 3850
Text HLabel 1700 4450 0    60   Input ~ 0
RX2
Text HLabel 1700 4600 0    60   Input ~ 0
TX2
Wire Wire Line
	1700 4600 1900 4600
Text Label 1900 4600 0    60   ~ 0
TX2
Text Label 1900 4450 0    60   ~ 0
RX2
Wire Wire Line
	1900 4450 1700 4450
Text Label 4200 3350 2    60   ~ 0
S_GND
Text Label 4200 3050 2    60   ~ 0
5V
$Comp
L ISO7221 U?
U 1 1 5A9F3403
P 4050 2450
F 0 "U?" H 4900 2200 60  0000 C CNN
F 1 "ISO7221" H 4900 2050 60  0000 C CNN
F 2 "" H 4050 2450 60  0000 C CNN
F 3 "" H 4050 2450 60  0000 C CNN
	1    4050 2450
	1    0    0    -1  
$EndComp
Text Label 5750 3050 0    60   ~ 0
iso_5V
Wire Wire Line
	5750 3050 5600 3050
Text Label 5800 3350 0    60   ~ 0
iso_gnd
Wire Wire Line
	5800 3350 5600 3350
Wire Wire Line
	4200 3050 4400 3050
Wire Wire Line
	4200 3350 4400 3350
Text Label 4200 3150 2    60   ~ 0
RX2
Text Label 4200 3250 2    60   ~ 0
TX2
Wire Wire Line
	4200 3150 4400 3150
Wire Wire Line
	4200 3250 4400 3250
Text Label 5750 3150 0    60   ~ 0
ISO_RX2
Wire Wire Line
	5600 3150 5750 3150
Text Label 5750 3250 0    60   ~ 0
ISO_TX2
Wire Wire Line
	5600 3250 5750 3250
Text Label 8350 3450 2    60   ~ 0
ISO_RX2
Wire Wire Line
	8350 3450 8650 3450
Text Label 4900 4750 2    60   ~ 0
S_GND
Text Label 11650 3800 0    60   ~ 0
S_GND
Text Label 8600 3950 0    60   ~ 0
ISO_TX2
Text HLabel 1600 5050 0    60   Input ~ 0
ISO_5V
Text HLabel 1600 5150 0    60   Input ~ 0
ISO_GND
Wire Wire Line
	1600 5050 1750 5050
Wire Wire Line
	1600 5150 1750 5150
Text Label 1750 5150 0    60   ~ 0
iso_gnd
Text Label 1750 5050 0    60   ~ 0
iso_5V
$EndSCHEMATC
