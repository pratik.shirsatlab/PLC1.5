EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:AUTOPLCV1.4-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 10
Title ""
Date "15 oct 2016"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 1300 1600 0    60   ~ 0
TX0
Text Label 1300 1700 0    60   ~ 0
RX0
Text Notes 2150 1700 0    60   ~ 0
2.54mm \nBerg/relimate
Text HLabel 1250 1700 0    60   Input ~ 0
RX0
Text HLabel 1250 1600 0    60   Input ~ 0
TX0
Text HLabel 1250 1800 0    60   Input ~ 0
SERIAL_GND
Text Label 1300 1800 0    60   ~ 0
GND
Text Label 7650 2050 2    60   ~ 0
ISP
Text Label 7700 1450 2    60   ~ 0
5V
Text Label 4050 2250 2    60   ~ 0
RESET
Text Label 4300 1600 2    60   ~ 0
5V
Text Label 6150 2300 0    60   ~ 0
GND
Text Label 4450 3550 0    60   ~ 0
GND
Text HLabel 1000 1100 0    60   Input ~ 0
5_volt
Text Label 1200 1100 0    60   ~ 0
5V
$Comp
L CONN_4 P9
U 1 1 5577EA20
P 1900 1650
F 0 "P9" V 1850 1650 50  0000 C CNN
F 1 "PROG" V 1950 1650 50  0000 C CNN
F 2 "AutomataFootprint:Pin_Header_Straight_1x04_Pitch2.54mm_Automata" H 1900 1650 60  0001 C CNN
F 3 "" H 1900 1650 60  0000 C CNN
	1    1900 1650
	1    0    0    -1  
$EndComp
Text Label 1450 1500 2    60   ~ 0
5V
Text HLabel 950  750  0    60   Input ~ 0
ISP
Text Label 1200 750  0    60   ~ 0
ISP
Text HLabel 950  900  0    60   Input ~ 0
RESET
Text Label 1200 900  0    60   ~ 0
RESET
Text Label 6150 5450 0    60   ~ 0
GND
$Comp
L R R1
U 1 1 5A0D0227
P 4550 5100
F 0 "R1" V 4630 5100 50  0000 C CNN
F 1 "10K" V 4550 5100 50  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMAT" V 4480 5100 50  0001 C CNN
F 3 "" H 4550 5100 50  0001 C CNN
	1    4550 5100
	1    0    0    -1  
$EndComp
Text Label 4550 4750 2    60   ~ 0
5V
Text HLabel 4350 5650 0    60   Input ~ 0
BOOTLOADER
$Comp
L LED D1
U 1 1 5A21644A
P 4300 2600
F 0 "D1" H 4300 2700 50  0000 C CNN
F 1 "LED" H 4300 2500 50  0000 C CNN
F 2 "AutomataFootprint:LED_1206" H 4300 2600 50  0001 C CNN
F 3 "" H 4300 2600 50  0001 C CNN
	1    4300 2600
	0    -1   -1   0   
$EndComp
$Comp
L R R2
U 1 1 5A7403C0
P 4300 1950
F 0 "R2" V 4380 1950 50  0000 C CNN
F 1 "1k" V 4300 1950 50  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMAT" V 4230 1950 50  0001 C CNN
F 3 "" H 4300 1950 50  0001 C CNN
	1    4300 1950
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 5A7405CE
P 4300 3150
F 0 "R3" V 4380 3150 50  0000 C CNN
F 1 "R330E" V 4300 3150 50  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMAT" V 4230 3150 50  0001 C CNN
F 3 "" H 4300 3150 50  0001 C CNN
	1    4300 3150
	1    0    0    -1  
$EndComp
$Comp
L C C30
U 1 1 5A740754
P 5400 1850
F 0 "C30" H 5425 1950 50  0000 L CNN
F 1 "100nF" H 5425 1750 50  0000 L CNN
F 2 "AutomataFootprint:AutomataFootprint_C_0805_AUTOMATA" H 5438 1700 50  0001 C CNN
F 3 "" H 5400 1850 50  0001 C CNN
	1    5400 1850
	0    1    1    0   
$EndComp
$Comp
L SW_DPST SW1
U 1 1 5A740B20
P 5400 2600
F 0 "SW1" H 5400 2800 50  0000 C CNN
F 1 "RESET" H 5400 2400 50  0000 C CNN
F 2 "AutomataFootprint:PUSH_BUTTON_AUTOMATA" H 5400 2600 50  0001 C CNN
F 3 "" H 5400 2600 50  0001 C CNN
	1    5400 2600
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 5A740FC9
P 7750 1800
F 0 "R4" V 7830 1800 50  0000 C CNN
F 1 "1k" V 7750 1800 50  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMAT" V 7680 1800 50  0001 C CNN
F 3 "" H 7750 1800 50  0001 C CNN
	1    7750 1800
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 5A74139D
P 7750 2350
F 0 "C4" H 7775 2450 50  0000 L CNN
F 1 "100nF" H 7775 2250 50  0000 L CNN
F 2 "AutomataFootprint:AutomataFootprint_C_0805_AUTOMATA" H 7788 2200 50  0001 C CNN
F 3 "" H 7750 2350 50  0001 C CNN
	1    7750 2350
	-1   0    0    1   
$EndComp
Text Notes 5100 1050 0    197  ~ 0
RESET
Text Notes 7850 1000 0    197  ~ 0
ISP
Text Notes 4350 4500 0    197  ~ 0
BOOTLOADER
$Comp
L SW_Push_Dual SW2
U 1 1 5AD4C4BC
P 5500 5350
F 0 "SW2" H 5550 5450 50  0000 L CNN
F 1 "BOOTLOADER_SWITCH" H 5500 5080 50  0000 C CNN
F 2 "AutomataFootprint:PUSH_BUTTON_AUTOMATA" H 5500 5550 50  0001 C CNN
F 3 "" H 5500 5550 50  0001 C CNN
	1    5500 5350
	1    0    0    -1  
$EndComp
Text Label 8650 2050 0    60   ~ 0
GND
Wire Wire Line
	1550 1800 1250 1800
Wire Wire Line
	1550 1700 1250 1700
Wire Wire Line
	1550 1600 1250 1600
Wire Wire Line
	7650 2050 7850 2050
Wire Wire Line
	8450 2050 8650 2050
Wire Wire Line
	7750 1950 7750 2200
Connection ~ 7750 2050
Wire Wire Line
	7750 1450 7750 1650
Wire Wire Line
	7750 1450 7700 1450
Wire Wire Line
	4300 1600 4300 1800
Wire Wire Line
	4050 2250 4900 2250
Wire Wire Line
	4300 2100 4300 2450
Connection ~ 4300 2250
Wire Wire Line
	4900 1850 4900 2750
Connection ~ 4900 2350
Wire Wire Line
	4900 1850 5250 1850
Connection ~ 4900 2250
Wire Wire Line
	5550 1850 5900 1850
Wire Wire Line
	5900 1850 5900 2750
Connection ~ 5900 2350
Wire Wire Line
	5900 2300 6150 2300
Connection ~ 5900 2300
Wire Wire Line
	4300 2750 4300 3000
Wire Wire Line
	4300 3550 4450 3550
Wire Wire Line
	4300 3300 4300 3550
Wire Wire Line
	1200 1100 1000 1100
Wire Wire Line
	8550 2050 8550 2750
Wire Wire Line
	8550 2750 7750 2750
Wire Wire Line
	7750 2750 7750 2500
Connection ~ 8550 2050
Wire Wire Line
	1450 1500 1550 1500
Wire Wire Line
	1200 750  950  750 
Wire Wire Line
	1200 900  950  900 
Wire Wire Line
	5950 5450 6150 5450
Wire Wire Line
	4350 5450 4350 5650
Wire Wire Line
	4550 5450 4550 5250
Connection ~ 4550 5450
Wire Wire Line
	4550 4950 4550 4750
Wire Wire Line
	4900 2350 5200 2350
Wire Wire Line
	5200 2350 5200 2500
Wire Wire Line
	4900 2750 5200 2750
Wire Wire Line
	5200 2750 5200 2700
Wire Wire Line
	5600 2500 5700 2500
Wire Wire Line
	5700 2500 5700 2350
Wire Wire Line
	5700 2350 5900 2350
Wire Wire Line
	5600 2700 5600 2750
Wire Wire Line
	5600 2750 5900 2750
Wire Notes Line
	3350 1100 6450 1100
Wire Notes Line
	6450 1100 6450 3950
Wire Notes Line
	6450 3950 3350 3950
Wire Notes Line
	3350 3950 3350 1100
Wire Notes Line
	7350 1100 9100 1100
Wire Notes Line
	9100 1100 9100 3150
Wire Notes Line
	9100 3150 7350 3150
Wire Notes Line
	7350 3150 7350 1100
Wire Notes Line
	3600 4550 6850 4550
Wire Notes Line
	6850 4550 6850 6100
Wire Notes Line
	6850 6100 3600 6100
Wire Notes Line
	3600 6100 3600 4550
Wire Wire Line
	5950 5550 5700 5550
Wire Wire Line
	5950 5350 5950 5550
Wire Wire Line
	5700 5350 5950 5350
Connection ~ 5950 5450
Wire Wire Line
	4350 5450 5000 5450
Wire Wire Line
	5000 5350 5000 5550
Wire Wire Line
	5000 5350 5300 5350
Wire Wire Line
	5000 5550 5300 5550
Connection ~ 5000 5450
$Comp
L JUMPER JP1
U 1 1 53BEDEE7
P 8150 2050
F 0 "JP1" H 8150 2200 60  0000 C CNN
F 1 "JUMPER" H 8150 1970 40  0000 C CNN
F 2 "AutomataFootprint:Automata_Pin_Header_Straight_2x01_Pitch2.54mm" H 8150 2050 60  0001 C CNN
F 3 "~" H 8150 2050 60  0000 C CNN
	1    8150 2050
	1    0    0    -1  
$EndComp
$EndSCHEMATC
