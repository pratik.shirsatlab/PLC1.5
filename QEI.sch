EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:automata
LIBS:automata_pwm
LIBS:CNCInterface-cache
LIBS:opendous
LIBS:FALCON_1769-cache
LIBS:FALCON_1769-rescue
LIBS:AUTOPLCV1.4-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 10
Title ""
Date "29 jul 2017"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5950 6000 0    60   Input ~ 0
5V
Text HLabel 5950 6250 0    60   Input ~ 0
GND
Text Label 6150 6000 0    60   ~ 0
5V
Text Label 6150 6250 0    60   ~ 0
GND
Wire Wire Line
	5950 6000 6150 6000
Wire Wire Line
	5950 6250 6150 6250
$Comp
L HCPL2631 OPT10
U 1 1 57AC4B81
P 7100 3200
F 0 "OPT10" H 7050 3500 60  0000 C CNN
F 1 "HCPL2631" H 7150 3000 60  0000 C CNN
F 2 "~" H 7100 3200 60  0000 C CNN
F 3 "~" H 7100 3200 60  0000 C CNN
	1    7100 3200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7350 3300 7400 3300
Wire Wire Line
	7400 3300 7400 4300
Wire Wire Line
	7350 2000 7350 3000
Wire Wire Line
	7350 2000 7750 2000
$Comp
L DIODE D14
U 1 1 57AC57D6
P 7500 2250
F 0 "D14" H 7500 2350 40  0000 C CNN
F 1 "1N4148" H 7500 2150 40  0000 C CNN
F 2 "~" H 7500 2250 60  0000 C CNN
F 3 "~" H 7500 2250 60  0000 C CNN
	1    7500 2250
	0    -1   -1   0   
$EndComp
$Comp
L DIODE D15
U 1 1 57AC57E3
P 7500 4050
F 0 "D15" H 7500 4150 40  0000 C CNN
F 1 "1N4148" H 7500 3950 40  0000 C CNN
F 2 "~" H 7500 4050 60  0000 C CNN
F 3 "~" H 7500 4050 60  0000 C CNN
	1    7500 4050
	0    1    1    0   
$EndComp
Wire Wire Line
	7400 4300 7750 4300
Wire Wire Line
	7500 2050 7500 2000
Connection ~ 7500 2000
Wire Wire Line
	7500 2550 7500 2450
Wire Wire Line
	7350 3100 7750 3100
Wire Wire Line
	7500 2850 7500 3100
Connection ~ 7500 3100
Wire Wire Line
	7350 3200 7750 3200
Wire Wire Line
	7500 3200 7500 3450
Connection ~ 7500 3200
Wire Wire Line
	7500 3750 7500 3850
Wire Wire Line
	7500 4250 7500 4300
Connection ~ 7500 4300
Text HLabel 6500 3100 0    40   Output ~ 0
PHASEB
Wire Wire Line
	6800 3100 6500 3100
Wire Wire Line
	8050 2000 8350 2000
Wire Wire Line
	8050 3100 8450 3100
Text HLabel 8350 2000 2    40   Output ~ 0
B+
Text HLabel 8450 3100 2    40   Output ~ 0
B-
Text HLabel 6500 3200 0    40   Output ~ 0
PHASEA
Text Label 8250 4300 0    40   ~ 0
A+
Text Label 8350 3200 0    40   ~ 0
A-
Wire Wire Line
	6500 3200 6800 3200
Wire Wire Line
	8350 4300 8050 4300
Text HLabel 8350 4300 2    40   Output ~ 0
A+
Text HLabel 8450 3200 2    40   Output ~ 0
A-
Text Label 4050 1900 0    40   ~ 0
I+
Text Label 4150 3000 0    40   ~ 0
I-
Wire Wire Line
	3850 3000 4250 3000
Wire Wire Line
	3750 1900 4150 1900
Text HLabel 2300 3000 0    40   Output ~ 0
INDEX
Text HLabel 4250 3000 2    40   Output ~ 0
I-
Text HLabel 4150 1900 2    40   Output ~ 0
I+
Wire Wire Line
	6800 3000 6650 3000
Wire Wire Line
	6650 3000 6650 2850
Wire Wire Line
	6800 3300 6650 3300
Wire Wire Line
	6650 3300 6650 3400
Text Label 6650 2850 0    60   ~ 0
5V
Text Label 6650 3400 2    60   ~ 0
GND
Wire Wire Line
	6800 3300 6800 3500
Wire Wire Line
	6800 3800 6800 3900
Wire Wire Line
	6800 3900 7100 3900
Wire Wire Line
	7100 3900 7100 2800
Wire Wire Line
	7100 2800 6800 2800
Wire Wire Line
	6800 2800 6800 3000
Wire Wire Line
	8050 3200 8450 3200
Text Label 8250 2000 2    40   ~ 0
B+
Text Label 8350 3100 2    40   ~ 0
B-
$Comp
L HCPL2631 OPT9
U 1 1 57ACB1E9
P 2900 3100
F 0 "OPT9" H 2850 3400 60  0000 C CNN
F 1 "HCPL2631" H 2950 2900 60  0000 C CNN
F 2 "~" H 2900 3100 60  0000 C CNN
F 3 "~" H 2900 3100 60  0000 C CNN
	1    2900 3100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3150 1900 3150 2900
Wire Wire Line
	3150 1900 3450 1900
$Comp
L DIODE D13
U 1 1 57ACB217
P 3300 2150
F 0 "D13" H 3300 2250 40  0000 C CNN
F 1 "1N4148" H 3300 2050 40  0000 C CNN
F 2 "~" H 3300 2150 60  0000 C CNN
F 3 "~" H 3300 2150 60  0000 C CNN
	1    3300 2150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3300 1950 3300 1900
Connection ~ 3300 1900
Wire Wire Line
	3300 2350 3300 2500
Wire Wire Line
	3150 3000 3550 3000
Wire Wire Line
	3300 2800 3300 3000
Connection ~ 3300 3000
Wire Wire Line
	2600 3000 2300 3000
Wire Wire Line
	2600 3200 2450 3200
Text Label 2450 2750 0    60   ~ 0
5V
Text Label 2450 3200 2    60   ~ 0
GND
Wire Wire Line
	2600 3200 2600 3450
Wire Wire Line
	2600 3750 2600 3800
Wire Wire Line
	2600 3800 2900 3800
Wire Wire Line
	2900 3800 2900 2700
Wire Wire Line
	2900 2700 2600 2700
Wire Wire Line
	2600 2700 2600 2900
NoConn ~ 3150 3100
NoConn ~ 3150 3200
NoConn ~ 2600 3100
Text HLabel 2650 6150 0    60   Input ~ 0
I+
Text HLabel 2650 6250 0    60   Input ~ 0
I-
Text HLabel 2650 5950 0    60   Input ~ 0
B+
Text HLabel 2650 6050 0    60   Input ~ 0
B-
Text HLabel 2650 5750 0    60   Input ~ 0
A+
Text HLabel 2650 5850 0    60   Input ~ 0
A-
Wire Wire Line
	2950 5750 2650 5750
Wire Wire Line
	2950 5850 2650 5850
Wire Wire Line
	2950 5950 2650 5950
Wire Wire Line
	2950 6050 2650 6050
Wire Wire Line
	2950 6150 2650 6150
Wire Wire Line
	2950 6250 2650 6250
Wire Wire Line
	2450 2750 2450 2900
Wire Wire Line
	2450 2900 2600 2900
$Comp
L C C11
U 1 1 5A7AAF98
P 2600 3600
F 0 "C11" H 2625 3700 50  0000 L CNN
F 1 "100nF" H 2625 3500 50  0000 L CNN
F 2 "" H 2638 3450 50  0001 C CNN
F 3 "" H 2600 3600 50  0001 C CNN
	1    2600 3600
	1    0    0    -1  
$EndComp
$Comp
L R R10
U 1 1 5A7AB3B6
P 3300 2650
F 0 "R10" V 3380 2650 50  0000 C CNN
F 1 "100E" V 3300 2650 50  0000 C CNN
F 2 "" V 3230 2650 50  0001 C CNN
F 3 "" H 3300 2650 50  0001 C CNN
	1    3300 2650
	1    0    0    -1  
$EndComp
$Comp
L R R12
U 1 1 5A7AB826
P 3700 3000
F 0 "R12" V 3780 3000 50  0000 C CNN
F 1 "100E" V 3700 3000 50  0000 C CNN
F 2 "" V 3630 3000 50  0001 C CNN
F 3 "" H 3700 3000 50  0001 C CNN
	1    3700 3000
	0    -1   -1   0   
$EndComp
$Comp
L R R11
U 1 1 5A7ABD59
P 3600 1900
F 0 "R11" V 3680 1900 50  0000 C CNN
F 1 "100E" V 3600 1900 50  0000 C CNN
F 2 "" V 3530 1900 50  0001 C CNN
F 3 "" H 3600 1900 50  0001 C CNN
	1    3600 1900
	0    -1   -1   0   
$EndComp
$Comp
L C C12
U 1 1 5A7AC2CD
P 6800 3650
F 0 "C12" H 6825 3750 50  0000 L CNN
F 1 "100nF" H 6825 3550 50  0000 L CNN
F 2 "" H 6838 3500 50  0001 C CNN
F 3 "" H 6800 3650 50  0001 C CNN
	1    6800 3650
	1    0    0    -1  
$EndComp
$Comp
L R R15
U 1 1 5A7AC90A
P 7900 2000
F 0 "R15" V 7980 2000 50  0000 C CNN
F 1 "100E" V 7900 2000 50  0000 C CNN
F 2 "" V 7830 2000 50  0001 C CNN
F 3 "" H 7900 2000 50  0001 C CNN
	1    7900 2000
	0    -1   -1   0   
$EndComp
$Comp
L R R13
U 1 1 5A7ACC70
P 7500 2700
F 0 "R13" V 7580 2700 50  0000 C CNN
F 1 "100E" V 7500 2700 50  0000 C CNN
F 2 "" V 7430 2700 50  0001 C CNN
F 3 "" H 7500 2700 50  0001 C CNN
	1    7500 2700
	1    0    0    -1  
$EndComp
$Comp
L R R16
U 1 1 5A7ACF69
P 7900 3100
F 0 "R16" V 7980 3100 50  0000 C CNN
F 1 "100E" V 7900 3100 50  0000 C CNN
F 2 "" V 7830 3100 50  0001 C CNN
F 3 "" H 7900 3100 50  0001 C CNN
	1    7900 3100
	0    -1   -1   0   
$EndComp
$Comp
L R R17
U 1 1 5A7AD0C9
P 7900 3200
F 0 "R17" V 7980 3200 50  0000 C CNN
F 1 "100E" V 7900 3200 50  0000 C CNN
F 2 "" V 7830 3200 50  0001 C CNN
F 3 "" H 7900 3200 50  0001 C CNN
	1    7900 3200
	0    1    1    0   
$EndComp
$Comp
L R R14
U 1 1 5A7AD1DD
P 7500 3600
F 0 "R14" V 7580 3600 50  0000 C CNN
F 1 "100E" V 7500 3600 50  0000 C CNN
F 2 "" V 7430 3600 50  0001 C CNN
F 3 "" H 7500 3600 50  0001 C CNN
	1    7500 3600
	1    0    0    -1  
$EndComp
$Comp
L R R18
U 1 1 5A7AD4C3
P 7900 4300
F 0 "R18" V 7980 4300 50  0000 C CNN
F 1 "100E" V 7900 4300 50  0000 C CNN
F 2 "" V 7830 4300 50  0001 C CNN
F 3 "" H 7900 4300 50  0001 C CNN
	1    7900 4300
	0    -1   -1   0   
$EndComp
$Comp
L Conn_01x06 J1
U 1 1 5A7B963B
P 3150 5950
F 0 "J1" H 3150 6250 50  0000 C CNN
F 1 "Conn_01x06" H 3150 5550 50  0000 C CNN
F 2 "" H 3150 5950 50  0001 C CNN
F 3 "" H 3150 5950 50  0001 C CNN
	1    3150 5950
	1    0    0    -1  
$EndComp
$EndSCHEMATC
