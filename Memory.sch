EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:AUTOPLCV1.4-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 10
Title ""
Date "15 oct 2016"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 7250 4200 2    60   Input ~ 0
SCL
Text HLabel 7250 4100 2    60   Input ~ 0
SDA
Text HLabel 1400 900  0    60   Input ~ 0
MEMORY_GND
Text HLabel 1400 1050 0    60   Input ~ 0
MEMORY_VCC
Text Label 1600 900  0    60   ~ 0
M_GND
Text Label 1600 1050 0    60   ~ 0
M_VCC
Text Label 6400 2700 0    60   ~ 0
M_VCC
Text Label 4350 4550 0    60   ~ 0
M_GND
Text Label 6400 3650 0    60   ~ 0
M_GND
$Comp
L 24LC16 U2
U 1 1 5A78B7EA
P 4850 3900
F 0 "U2" H 4600 4150 50  0000 C CNN
F 1 "24LC16" H 4900 4150 50  0000 L CNN
F 2 "AutomataFootprint1:SO8E" H 4900 3650 50  0001 L CNN
F 3 "" H 4850 3800 50  0001 C CNN
	1    4850 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 3800 4350 3800
Wire Wire Line
	4450 3900 4350 3900
Wire Wire Line
	4350 4000 4450 4000
Wire Wire Line
	4350 3800 4350 4550
Connection ~ 4350 3900
Connection ~ 4350 4000
Wire Wire Line
	4850 3050 6800 3050
Wire Wire Line
	6400 2700 6400 3150
Connection ~ 6400 3050
Wire Wire Line
	6400 3450 6400 3650
Wire Wire Line
	5850 4100 7250 4100
Wire Wire Line
	5650 4200 7250 4200
Wire Wire Line
	5250 4500 4350 4500
Connection ~ 4350 4500
Wire Wire Line
	6750 3500 6900 3500
Wire Wire Line
	6800 3050 6800 3500
Connection ~ 6800 3500
Wire Wire Line
	6750 3900 6750 4100
Connection ~ 6750 4100
Wire Wire Line
	6900 3900 6900 4200
Connection ~ 6900 4200
Wire Wire Line
	1400 900  1600 900 
Wire Wire Line
	1400 1050 1600 1050
Wire Wire Line
	4850 3050 4850 3600
$Comp
L R R9
U 1 1 5A78BDA7
P 6900 3750
F 0 "R9" V 6980 3750 50  0000 C CNN
F 1 "10k" V 6900 3750 50  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMAT" V 6830 3750 50  0001 C CNN
F 3 "" H 6900 3750 50  0001 C CNN
	1    6900 3750
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 5A78BDF4
P 6750 3750
F 0 "R8" V 6830 3750 50  0000 C CNN
F 1 "10k" V 6750 3750 50  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMAT" V 6680 3750 50  0001 C CNN
F 3 "" H 6750 3750 50  0001 C CNN
	1    6750 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 3600 6750 3500
Wire Wire Line
	6900 3500 6900 3600
$Comp
L C C35
U 1 1 5A78BF28
P 6400 3300
F 0 "C35" H 6425 3400 50  0000 L CNN
F 1 "100nF" H 6425 3200 50  0000 L CNN
F 2 "AutomataFootprint:AutomataFootprint_C_0805_AUTOMATA" H 6438 3150 50  0001 C CNN
F 3 "" H 6400 3300 50  0001 C CNN
	1    6400 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 4000 5250 4500
Wire Wire Line
	5250 3900 5650 3900
Wire Wire Line
	5650 3900 5650 4200
Wire Wire Line
	5250 3800 5850 3800
Wire Wire Line
	5850 3800 5850 4100
Connection ~ 5250 4400
Wire Wire Line
	4850 4200 4850 4500
Connection ~ 4850 4500
$EndSCHEMATC
