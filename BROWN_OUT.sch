EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:AUTOPLCV1.4-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 10
Title ""
Date "29 jul 2017"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R R149
U 1 1 597236CE
P 5500 3250
F 0 "R149" V 5580 3250 40  0000 C CNN
F 1 "560E" V 5507 3251 40  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMAT" V 5430 3250 30  0001 C CNN
F 3 "~" H 5500 3250 30  0000 C CNN
	1    5500 3250
	1    0    0    -1  
$EndComp
$Comp
L R R148
U 1 1 597236D5
P 5200 2900
F 0 "R148" V 5280 2900 40  0000 C CNN
F 1 "2.2K" V 5207 2901 40  0000 C CNN
F 2 "AutomataFootprint:R_0805_AUTOMAT" V 5130 2900 30  0001 C CNN
F 3 "~" H 5200 2900 30  0000 C CNN
	1    5200 2900
	0    -1   -1   0   
$EndComp
$Comp
L C C57
U 1 1 597236E3
P 6050 3200
F 0 "C57" H 6050 3300 40  0000 L CNN
F 1 "1n" H 6056 3115 40  0000 L CNN
F 2 "AutomataFootprint:C_0805_AUTOMATA" H 6088 3050 30  0001 C CNN
F 3 "~" H 6050 3200 60  0000 C CNN
	1    6050 3200
	1    0    0    -1  
$EndComp
Text Label 4300 2900 2    60   ~ 0
24V
Text Label 6300 3000 0    60   ~ 0
BROWN_OUT
Text Label 4300 3550 0    60   ~ 0
GND
Wire Wire Line
	5800 3550 5800 3350
Connection ~ 5800 3550
Connection ~ 5500 3550
Connection ~ 6050 3550
Wire Wire Line
	4300 2900 4450 2900
Text HLabel 1700 1350 0    60   Input ~ 0
24V
Text HLabel 1700 1500 0    60   Input ~ 0
GND
Text HLabel 1700 1650 0    60   Input ~ 0
BROWN_OUT
Wire Wire Line
	1700 1350 2300 1350
Wire Wire Line
	1700 1500 2300 1500
Wire Wire Line
	1700 1650 2300 1650
Text Label 2300 1350 0    60   ~ 0
24V
Text Label 2300 1500 0    60   ~ 0
GND
Text Label 2300 1650 0    60   ~ 0
BROWN_OUT
Wire Wire Line
	4750 2900 5050 2900
Wire Wire Line
	5350 2900 6300 2900
Wire Wire Line
	6300 2900 6300 3000
Wire Wire Line
	5800 2900 5800 3050
Connection ~ 5800 2900
Wire Wire Line
	6050 2900 6050 3050
Connection ~ 6050 2900
Wire Wire Line
	6050 3550 6050 3350
Wire Wire Line
	5500 3400 5500 3550
Wire Wire Line
	5500 2900 5500 3100
Connection ~ 5500 2900
$Comp
L D_Zener D43
U 1 1 5A217EB6
P 4600 2900
F 0 "D43" H 4600 3000 50  0000 C CNN
F 1 "1N4740" H 4600 2800 50  0000 C CNN
F 2 "AutomataFootprint:SOD123" H 4600 2900 50  0001 C CNN
F 3 "" H 4600 2900 50  0001 C CNN
	1    4600 2900
	1    0    0    -1  
$EndComp
$Comp
L D_Zener D44
U 1 1 5A217F68
P 5800 3200
F 0 "D44" H 5800 3300 50  0000 C CNN
F 1 "1N4730" H 5800 3100 50  0000 C CNN
F 2 "AutomataFootprint:SOD123" H 5800 3200 50  0001 C CNN
F 3 "" H 5800 3200 50  0001 C CNN
	1    5800 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	4300 3550 6050 3550
$EndSCHEMATC
